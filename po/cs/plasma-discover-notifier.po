# Copyright (C) YEAR This_file_is_part_of_KDE
# This file is distributed under the same license as the PACKAGE package.
# Vít Pelčák <vit@pelcak.org>, 2015, 2018, 2019, 2020.
# Vit Pelcak <vit@pelcak.org>, 2021, 2023.
#
msgid ""
msgstr ""
"Project-Id-Version: \n"
"Report-Msgid-Bugs-To: https://bugs.kde.org\n"
"POT-Creation-Date: 2024-01-10 01:35+0000\n"
"PO-Revision-Date: 2023-08-10 15:49+0200\n"
"Last-Translator: Vit Pelcak <vit@pelcak.org>\n"
"Language-Team: Czech <kde-i18n-doc@kde.org>\n"
"Language: cs\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=3; plural=(n==1) ? 0 : (n>=2 && n<=4) ? 1 : 2;\n"
"X-Generator: Lokalize 23.04.3\n"

#: notifier/DiscoverNotifier.cpp:154
#, kde-format
msgid "View Updates"
msgstr "Prohlédnout aktualizace"

#: notifier/DiscoverNotifier.cpp:263
#, kde-format
msgid "Security updates available"
msgstr "Jsou dostupné bezpečnostní aktualizace"

#: notifier/DiscoverNotifier.cpp:265
#, kde-format
msgid "Updates available"
msgstr "Jsou dostupné aktualizace"

#: notifier/DiscoverNotifier.cpp:267
#, kde-format
msgid "System up to date"
msgstr "Systém je aktuální"

#: notifier/DiscoverNotifier.cpp:269
#, kde-format
msgid "Computer needs to restart"
msgstr "Je potřeba restartovat počítač"

#: notifier/DiscoverNotifier.cpp:271
#, kde-format
msgid "Offline"
msgstr "Odpojen"

#: notifier/DiscoverNotifier.cpp:273
#, kde-format
msgid "Applying unattended updates…"
msgstr ""

#: notifier/DiscoverNotifier.cpp:297
#, kde-format
msgid "Restart is required"
msgstr "Je vyžadován restart"

#: notifier/DiscoverNotifier.cpp:298
#, kde-format
msgid "The system needs to be restarted for the updates to take effect."
msgstr ""
"K provedení změn bude potřeba restartovat počítač po dokončení této "
"aktualizace."

#: notifier/DiscoverNotifier.cpp:303
#, kde-format
msgctxt "@action:button"
msgid "Restart"
msgstr "Restartovat"

#: notifier/DiscoverNotifier.cpp:328
#, kde-format
msgid "Upgrade available"
msgstr "Dostupná aktualizace distribuce"

#: notifier/DiscoverNotifier.cpp:329
#, kde-format
msgctxt "A new distro release (name and version) is available for upgrade"
msgid "%1 is now available."
msgstr "%1 je nyní dostupný."

#: notifier/DiscoverNotifier.cpp:332
#, kde-format
msgctxt "@action:button"
msgid "Upgrade"
msgstr "Aktualizovat"

#: notifier/main.cpp:38
#, kde-format
msgid "Discover Notifier"
msgstr "Upozornění Discover"

#: notifier/main.cpp:40
#, kde-format
msgid "System update status notifier"
msgstr "Upozornění na stav aktualizace systému"

#: notifier/main.cpp:42
#, kde-format
msgid "© 2010-2024 Plasma Development Team"
msgstr ""

#: notifier/main.cpp:46
#, kde-format
msgctxt "NAME OF TRANSLATORS"
msgid "Your names"
msgstr "Vít Pelčák"

#: notifier/main.cpp:46
#, kde-format
msgctxt "EMAIL OF TRANSLATORS"
msgid "Your emails"
msgstr "vit@pelcak.org"

#: notifier/main.cpp:51
#, kde-format
msgid "Replace an existing instance"
msgstr "Nahradit existující instanci"

#: notifier/main.cpp:53
#, kde-format
msgid "Do not show the notifier"
msgstr "Nezobrazovat upozornění"

#: notifier/main.cpp:53
#, kde-format
msgid "hidden"
msgstr "skryté"

#: notifier/NotifierItem.cpp:22 notifier/NotifierItem.cpp:23
#, kde-format
msgid "Updates"
msgstr "Aktualizace"

#: notifier/NotifierItem.cpp:35
#, kde-format
msgid "Open Discover…"
msgstr "Otevřít Discover…"

#: notifier/NotifierItem.cpp:40
#, kde-format
msgid "See Updates…"
msgstr "Přehled aktualizací…"

#: notifier/NotifierItem.cpp:45
#, kde-format
msgid "Refresh…"
msgstr "Obnovit…"

#: notifier/NotifierItem.cpp:49
#, kde-format
msgid "Restart to apply installed updates"
msgstr "Restartujte pro použití nainstalovaných aktualizací"

#: notifier/NotifierItem.cpp:50
#, kde-format
msgid "Click to restart the device"
msgstr "Kliknutím restartovat počítač"
